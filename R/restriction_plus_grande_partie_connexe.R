

#' restriction_plus_grande_partie_connexe
#' algo skater faisable uniquement sur un territoire conexxe
#' donc on restreint le territoire à sa plus grande partie connexe
#'
#' @param table table de donnees
#' @param composantes_connexes toutes les composantes connexes
#'
#' @return renvoie la composante connexe la plus grande et supprime les autres composantes
#' @export
#'

restriction_plus_grande_partie_connexe = function (table,composantes_connexes){
Freq=NULL #pour eviter le warning du check package
desc=NULL #pour eviter le warning du check package

  # table des composantes connexes
  t1 <-table(composantes_connexes$comp.id) %>%  as.data.frame() %>%  dplyr::arrange(desc(Freq))

  #taille  de la plus grande partie connexe
  t1[1,1]

  #restriction de la table (map et donnees)
  return(table[composantes_connexes$comp.id==t1[1,1],])


}
