# L’indice de Miller varie de 0 (forme linéaire) à 1 (forme circulaire), tout comme l’indice de Morton. Ainsi pour
# ces deux indices, plus la forme est régulière plus l’indice se rapproche de 1, à la limite un cercle est un polygone
# régulier à n côtés avec n tendant vers l’infini (l’aire d’un cercle a d’ailleurs été découverte par ce procédé).
# L’indice de Gravélius quant à lui, varie dans le sens inverse des deux autres indices, de 1 à l’infini
# Ainsi plus une forme est compacte, plus son indice tend vers 1.
# https://www.unil.ch/files/live/sites/ouvdd/files/shared/Colloque%202005/Communications/A)%20Ecologie%20urbaine/A1/G.%20Maignant.pdf


#' indice_miller
#'
#' @param polygone polygone
#'
#' @return indice de Miller pour un polygone
#' @export
#'

indice_miller=function(polygone){
  #calcul de l'aire
  A<-sf::st_area(polygone) # necessite package lwgeom
  # Calculate polygon(s) perimeter
  P=sf::st_union(polygone) %>%  sf::st_cast("MULTILINESTRING") %>% sf::st_length()
  #indices de compacité
  return(4*3.14*sum(A)/(P^2))
}

