
<!-- README.md is generated from README.Rmd. Please edit that file -->

# spclust

<!-- badges: start -->

<!-- badges: end -->

Le package spclust permet de comparer et de visualiser plusieurs
méthodes de classification sous contrainte géographique (spatially
constrained clustering). Méthodes disponibles: Kmeans, Kmeans variante
xy, CAH Clustgeo, Skater, SOM.

<!-- ## Installation -->

<!-- You can install the released version of spclust from [CRAN](https://CRAN.R-project.org) with: -->

<!-- ``` r -->

<!-- install.packages("spclust") -->

<!-- ``` -->

<!-- ## Example -->

<!-- This is a basic example which shows you how to solve a common problem: -->

<!-- ```{r example} -->

<!-- library(spclust) -->

<!-- ## basic example code -->

<!-- ``` -->
